#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Invoice Line Description Update',
    'name_de_DE': 'Rechnungsposition Aktualisierung Beschreibung',
    'name': 'Name to specify for account_invoice_line_description_update',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Updates the description of an invoice line on change of a product
''',
    'description_de_DE': '''
    - Aktualisiert die Beschreibung einer Rechnungsposition, wenn das Produkt
      einer Position geändert wird.
''',
    'depends' : [
        'account_invoice',
    ],
    'xml' : [
    ],
    'translation': [
    ],
}
