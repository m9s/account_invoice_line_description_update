#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction
from trytond.pool import Pool


class InvoiceLine(ModelSQL, ModelView):
    "Invoice Line"
    _description = __doc__
    _name = "account.invoice.line"

    def on_change_product(self, vals):
        party_obj = Pool().get('party.party')
        product_obj = Pool().get('product.product')

        res = super(InvoiceLine, self).on_change_product(vals)

        if not vals.get('product'):
            return res

        if vals.get('_parent_invoice.party'):
            party = party_obj.browse(vals['_parent_invoice.party'])
            if party.lang:
                context_language = party.lang.code

        # remove product code from description
        product = product_obj.browse(vals['product'])
        with Transaction().set_context(language=context_language):
            res['description'] = product_obj.browse(product.id).name

        return res

InvoiceLine()
